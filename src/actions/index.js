import localhost from "../apis/localhost";
const visable = false;
export const fetchPosts = () => {
    return async dispatch => {
        const responce = await localhost.get('/community');
    
        dispatch({
            type: 'FETCH_POSTS',
            payload: responce.data
        });

    };
};

export const visibility = () => {
    return {
        type: 'VISABLE',
        payload: !visable
    }
}
export const subscribed = (option) => {
    return {
        type: 'SUBSCRIBE',
        payload: option
    }
}
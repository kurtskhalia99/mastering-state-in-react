import './App.css';
import Community from './Community';
import JoinProgram from './JoinProgram';

function App() {
  return (
    <div id="app-container">
      <Community />
      <JoinProgram />
    </div>
  );
}

export default App;

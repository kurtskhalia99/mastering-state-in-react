import React, { useState } from 'react';
import './JoinProgram.css';

import { connect } from 'react-redux';
import { subscribed } from "../actions";

function JoinProgram(props) {
    const [email, setEmail] = useState('');
    const [subscribed, setSubscribed] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const fetchDataSubscribe = () => {
        fetch('http://localhost:3000/subscribe', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({email: email}),
          })
          .then((response) => {
            if(response.status === 422){
              response.json().then(data => window.alert(data.error));
            } else {
            return response.json();
            }
          })
          .then(data => {
            setIsLoading(false);
            props.subscribed(true);
            setEmail('');
          })
          .catch((error) => {
            setIsLoading(false);
            console.error('Error:', error);
          });
    }
    
    const fetchDataUnsubscribe = () => {
        fetch('http://localhost:3000/unsubscribe', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
          })
          .then((response) => response.json())
          .then(data => {
            setIsLoading(false);
            props.subscribed(false);;
          })
          .catch((error) => {
            setIsLoading(false);
            console.error('Error:', error);
          });
    } 


    const onSubmit = (event) => {
        event.preventDefault();
        setIsLoading(true);
        if(props.subscribe.subscribe) {
            fetchDataUnsubscribe();
        } else {
            fetchDataSubscribe();
        }
    }

    return ( 
        <section className='app-section app-section--join-our-program'>
            <h1 className='app-title'>Join Our Program</h1>
            <h3 className='app-subtitle'>Sed do eiusmod tempor incididunt ut <br></br> labore et dolore magna aliqua.</h3>
            <form onSubmit={onSubmit}>
                {!props.subscribe.subscribe ? <>
                    <input
                        type='email' 
                        className='app-input' 
                        value={email}
                        onChange={(event) => setEmail(event.target.value)}>
                    </input>
                    <button type='submit' disabled={isLoading} style={isLoading? {opacity: 0.5}: {}} className='app-section__button app-section__button--read-more'>Subscribe</button>
                </> : <button type='submit' disabled={isLoading} style={isLoading? {opacity: 0.5}: {}} className='app-section__button app-section__button--read-more'>UNSUBSCRIBE</button>}
            </form> 
        </section>
     );
}

const mapStateToProps = (state) => {
  return {subscribe: state.subscribe}
}

export default connect(mapStateToProps, { subscribed })(JoinProgram);
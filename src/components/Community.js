import React, { useEffect, useState } from 'react';
import './Community.css'

import { connect } from 'react-redux';
import { fetchPosts, visibility } from "../actions";

class PostList extends React.Component {
    componentDidMount() {
        this.props.fetchPosts();
    }

    render() {
        return (
            <section className='app-section app-section--big-community'>
                <div className='visibility'>
                    <button onClick={() => this.props.visibility()} className='app-section__button--visibility'>{ this.props.visable.visable? "Hide Section" : 'Show section'}</button>
                    <h2 className="app-title">
                        Big Community of <br />People Like You 
                    </h2>
                </div>
                <h3 className="app-subtitle">
                    We are proud of our products, and we are really excited 
                    <br />when we get feedback from our users.
                </h3 >
                {
                    this.props.visable.visable? <div className='app-community'>
                                                    {
                                                        this.props.costumers.map((costumer) => {
                                                            return (
                                                                <div key={costumer.id} className='app-community--avatar'>
                                                                    <img src={costumer.avatar}></img>
                                                                    <p>Random text</p>
                                                                    <h6>{`${costumer.firstName} ${costumer.lastName}`}</h6>
                                                                    <p className='app-community--avatar-position'>{costumer.position}</p>
                                                                </div>
                                                            );
                                                        })
                                                    }
                                                </div> : <div></div>
                }
                
            </section>
        );
    };
};

const mapStateToProps = (state) => {
    return {costumers: state.costumers, visable: state.visable}
}

export default connect(mapStateToProps, { fetchPosts, visibility })(PostList);

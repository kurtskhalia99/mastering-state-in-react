import {combineReducers} from 'redux';
const INITIAL_STATE = {
    visable: true
}
const INITIAL_STATE2 = {
    subscribe: false
}

const userData = (state = [], action) => {
    switch (action.type) {
        case 'FETCH_POSTS':
            return action.payload;
        default:
            return state;
    }
}

const visibility = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'VISABLE':
            return {
                ...state,
                visable: !state.visable
            }
        default:
            return state;
    }
}
const subscribed = (state = INITIAL_STATE2, action) => {
    switch (action.type) {
        case 'SUBSCRIBE':
            return {
                ...state,
                subscribe: !state.subscribe
            }
        default:
            return state;
    }
}

export default combineReducers({costumers: userData, visable: visibility, subscribe: subscribed});